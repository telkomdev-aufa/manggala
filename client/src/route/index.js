
import Home from '../pages/Home.vue'
import Admin from '../pages/Admin.vue'

export const route = [
  {
    name: 'home',
    path: '/',
    component: Home
  },
  {
    name: 'admin',
    path: '/admin',
    component: Admin
  }
]
