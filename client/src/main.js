import Vue from 'vue'
import App from './App.vue'
import vueRouter from 'vue-router'
import axios from 'axios'

Vue.prototype.axios = axios
Vue.use(vueRouter)

import { route } from './route/index'

Vue.config.productionTip = false

const router = new vueRouter({ mode: 'history', routes: route});
new Vue(Vue.util.extend({ router }, App)).$mount("#app");
