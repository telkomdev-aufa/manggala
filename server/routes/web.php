<?php

use App\Helpers\Wrapper;

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/

$router->get('api/v1/ping', function () { return Wrapper::sendResponse('success', "pong"); });

$router->post('api/v1/login', ['middleware' => 'basicAuth', 'uses' => 'AuthController@Login']);
$router->post('api/v1/user', ['middleware' => 'basicAuth', 'uses' => 'UserController@CreateUser']);
$router->get('api/v1/article', ['middleware' => 'basicAuth', 'uses' => 'ArticleController@GetArticle']);

// $router->get('api/v1/user', ['middleware' => 'auth', 'uses' => 'UserController@GetUser']);
// $router->get('api/v1/user/{userId}', ['middleware' => 'auth', 'uses' => 'UserController@GetUserById']);
// $router->put('api/v1/user/{userId}', ['middleware' => 'auth', 'uses' => 'UserController@EditUser']);
// $router->delete('api/v1/user/delete/{userId}', ['middleware' => 'auth', 'uses' => 'UserController@DeleteUser']);

$router->post('api/v1/article', ['middleware' => 'basicAuth', 'uses' => 'ArticleController@CreateArticle']);
$router->put('api/v1/article/{id}', ['middleware' => 'auth', 'uses' => 'ArticleController@EditArticle']);

