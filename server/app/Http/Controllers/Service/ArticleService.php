<?php

namespace App\Http\Controllers\Service;

use App\Models\Article;
use App\Helpers\Wrapper;
use Illuminate\Http\Request;

class ArticleService {
  // ===================================================================
  // QUERY
  // ===================================================================
  public function getAll() {
    try {
      $query = Article::all();
    } catch (\Exception $e) {
      return Wrapper::error($e->getMessage());
    }
    return Wrapper::data($query);
  }

  public function findOneById($payload) {
    try {
      $query = Article::where('id', $payload)->first();
    } catch (\Exception $e) {
      return Wrapper::error($e->getMessage());
    }
    return Wrapper::data($query);
  }

  public function findManyByName($payload) {
    try {
      $query = Article::where('name', $payload)->get();
    } catch (\Exception $e) {
      return Wrapper::error($e->getMessage());
    }
    return Wrapper::data($query);
  }
  

  // ===================================================================
  // COMMAND
  // ===================================================================
  public function insertOne($payload) {
    try {
      $query = Article::create($payload);
    } catch (\Exception $e) {
      return Wrapper::throw($e->getMessage());
    }
    return Wrapper::data($query);
  }

  public function updateOne($payload) {
    try {
      $query = Article::where('id', $payload['id']);
      $query->update($payload['data']);
    } catch (\Throwable $e) {
      return Wrapper::throw($e->getMessage());
    }
    return Wrapper::data($query);
  }

  public function deleteOne($payload) {
    try {
      $query = Article::where('id', $payload);
      $query->delete();
    } catch (\Throwable $e) {
      return Wrapper::throw($e->getMessage());
    }
    return Wrapper::data($query);
  }
}
