<?php

namespace App\Http\Controllers\Service;

use App\Models\User;
use App\Helpers\Wrapper;
use Illuminate\Http\Request;

class UserService {
  // ===================================================================
  // QUERY
  // ===================================================================
  public function getAll() {
    try {
      $query = User::all();
    } catch (\Exception $e) {
      return Wrapper::error($e->getMessage());
    }
    return Wrapper::data($query);
  }

  public function findById($payload) {
    try {
      $query = User::where('userId', $payload)->first();
    } catch (\Exception $e) {
      return Wrapper::error($e->getMessage());
    }
    return Wrapper::data($query);
  }
  

  // ===================================================================
  // COMMAND
  // ===================================================================
  public function insertOne($payload) {
    try {
      $query = User::create($payload);
    } catch (\Exception $e) {
      return Wrapper::throw($e->getMessage());
    }
    return Wrapper::data($query);
  }

  public function updateOne($payload) {
    try {
      $query = User::where('userId', $payload['userId']);
      $query->update($payload['data']);
    } catch (\Throwable $e) {
      return Wrapper::throw($e->getMessage());
    }
    return Wrapper::data($query);
  }

  public function deleteOne($payload) {
    try {
      $query = User::where('userId', $payload);
      $query->delete();
    } catch (\Throwable $e) {
      return Wrapper::throw($e->getMessage());
    }
    return Wrapper::data($query);
  }
}
