<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Helpers\Massage;
use App\Helpers\ErrorCode;
use App\Helpers\HttpError;
use App\Helpers\Wrapper;
use App\Http\Controllers\Schema\UserSchema;
use App\Http\Controllers\Service\UserService;
use Illuminate\Support\Facades\Hash;

class UserController extends Controller
{
    public function __construct() {
        $this->userService = new UserService();
    }


  // ===================================================================
  // QUERY
  // ===================================================================
    function GetUser () {
        dd(auth()->user());
        $query = $this->userService->getAll()->getData();
        $result = $query->data;
        return Wrapper::sendResponse('success', $result);
    }

    function GetUserById ($userId) {
        $query = $this->userService->findById($userId)->getData();
        if (!$query->data) {
            return Wrapper::sendResponse('fail', '', Massage::USER_NOT_FOUND, HttpError::NOT_FOUND);
        }
        $result = $query->data;
        return Wrapper::sendResponse('success', $result);
    }

    
  // ===================================================================
  // COMMAND
  // ===================================================================
    function CreateUser (Request $req) {
        $data = $req->all();
        $payload = UserSchema::schemaInsertUser($data);
        $payload['password'] = Hash::make($payload['password']);

        $query = $this->userService->insertOne($payload)->getData();
        if ($query->err) {
            return Wrapper::sendResponse('fail', '', $query->message, $query->code);
        }
        $result = $query->data;
        return Wrapper::sendResponse('success', $result);
    }

    function EditUser ($userId, Request $req) {
        $data = $req->all();
        $data = [
            'userId' => $userId,
            'data' => $data
        ];
        $payload = UserSchema::schemaEditUser($data);

        $findUser = $this->userService->findById($userId)->getData();
        if (!$findUser->data) {
            return Wrapper::sendResponse('fail', '', Massage::USER_NOT_FOUND, HttpError::NOT_FOUND);
        }
        $query = $this->userService->updateOne($payload)->getData();
        $result = $query->data;
        return Wrapper::sendResponse('success', $result);
    }

    function DeleteUser ($userId) {
        $findUser = $this->userService->findById($userId)->getData();
        if (!$findUser->data) {
            return Wrapper::sendResponse('fail', '', Massage::USER_NOT_FOUND, HttpError::NOT_FOUND);
        }
        $query = $this->userService->deleteOne($userId)->getData();
        $result = $query->data;
        return Wrapper::sendResponse('success', $result);
    }
}
