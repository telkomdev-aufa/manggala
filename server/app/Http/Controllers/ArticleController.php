<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Helpers\Massage;
use App\Helpers\ErrorCode;
use App\Helpers\HttpError;
use App\Helpers\Wrapper;
use App\Http\Controllers\Schema\articleSchema;
use App\Http\Controllers\Service\articleService;
use Illuminate\Support\Facades\Hash;

class ArticleController extends Controller
{
    public function __construct() {
        $this->articleService = new articleService();
    }


  // ===================================================================
  // QUERY
  // ===================================================================
    function GetArticle () {
        // $query = $this->articleService->getAll()->getData();
        // $result = $query->data;

        $jumbotron = $this->articleService->findManyByName('jumbotron')->getData();
        $service = $this->articleService->findManyByName('service')->getData();
        $plan = $this->articleService->findManyByName('plan')->getData();
        $featureText = $this->articleService->findManyByName('featureText')->getData();
        $feature = $this->articleService->findManyByName('feature')->getData();
        $footer = $this->articleService->findManyByName('footer')->getData();

        $result = (object) [
            'jumbotron' => $jumbotron->data,
            'service' => $service->data,
            'plan' => $plan->data,
            'featureText' => $featureText->data,
            'feature' => $feature->data,
            'footer' => $footer->data,
        ];

        return Wrapper::sendResponse('success', $result);
    }

    
  // ===================================================================
  // COMMAND
  // ===================================================================
    function CreateArticle (Request $req) {
        $data = $req->all();
        $payload = articleSchema::schemaInsertarticle($data);

        $query = $this->articleService->insertOne($payload)->getData();
        if ($query->err) {
            return Wrapper::sendResponse('fail', '', $query->message, $query->code);
        }
        $result = $query->data;
        return Wrapper::sendResponse('success', $result);
    }

    function EditArticle ($id, Request $req) {
        $data = $req->all();
        $data = [
            'id' => $id,
            'data' => $data
        ];
        $payload = articleSchema::schemaEditarticle($data);

        $findarticle = $this->articleService->findOneById($id)->getData();
        if (!$findarticle->data) {
            return Wrapper::sendResponse('fail', '', Massage::article_NOT_FOUND, HttpError::NOT_FOUND);
        }
        $query = $this->articleService->updateOne($payload)->getData();
        $result = $query->data;
        return Wrapper::sendResponse('success', $result);
    }

    function DeleteArticle ($articleId) {
        $findarticle = $this->articleService->findById($articleId)->getData();
        if (!$findarticle->data) {
            return Wrapper::sendResponse('fail', '', Massage::article_NOT_FOUND, HttpError::NOT_FOUND);
        }
        $query = $this->articleService->deleteOne($articleId)->getData();
        $result = $query->data;
        return Wrapper::sendResponse('success', $result);
    }
}
