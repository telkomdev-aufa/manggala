<?php

namespace App\Http\Controllers;

use Laravel\Lumen\Routing\Controller as BaseController;

class Controller extends BaseController
{
    public function responset($data) {
        $response = (object) [    
            'code' => 200,
            'data' => $data,
            'massage' => 'Your Request Has Been Processed',
            'success' => true
        ];
        return response() -> json($response, 200);
    }

}
