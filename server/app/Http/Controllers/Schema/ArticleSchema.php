<?php

namespace App\Http\Controllers\Schema;

use App\Helpers\Wrapper;
use Illuminate\Support\Facades\Validator;

class ArticleSchema {
    public static function schemaInsertArticle($payload) {
        $validation = Validator::make($payload, [
            'name' => 'required',
            'title' => 'nullable',
            'content' => 'required',
            'icon' => 'nullable',
            'isActive' => 'nullable'
        ]);
        if ($validation->fails()) {
            return Wrapper::throw($validation->errors()->first(), 400);
        }

        return $validation->validated();
    }

    public static function schemaEditArticle($payload) {
        $validation = Validator::make($payload, [
            'id' => 'required',
            'data' => 'present|array',
            'data.name' => 'nullable',
            'data.title' => 'nullable',
            'data.content' => 'nullable',
            'data.icon' => 'nullable',
            'data.isActive' => 'nullable'
        ]);
        if ($validation->fails()) {
            return Wrapper::throw($validation->errors()->first(), 400);
        }

        $result = $validation->validated();
        unset($result['data']['_method']);
        return $result;
    }
}