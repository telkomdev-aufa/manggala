<?php

namespace App\Http\Controllers\Schema;

use App\Helpers\Wrapper;
use Illuminate\Support\Facades\Validator;

class UserSchema {
    public static function schemaInsertUser($payload) {
        $validation = Validator::make($payload, [
            'username' => 'required|unique:users',
            'password' => 'required',
        ]);
        if ($validation->fails()) {
            return Wrapper::throw($validation->errors()->first(), 400);
        }

        return $validation->validated();
    }
}