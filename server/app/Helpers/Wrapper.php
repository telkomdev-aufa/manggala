<?php
namespace App\Helpers;

use App\Helpers\ErrorCode;

class Wrapper {
    public static function data($data) {
        $response = [    
            'err' => null,
            'data' => $data,
            'message' => '',
            'code' => 200
        ];
        return response() -> json($response, 200);
    }
    
    public static function error($message, $code=HttpError::INTERNAL_ERROR) {
        $response = [    
            'err' => true,
            'data' => '',
            'message' => $message,
            'code' => $code
        ];
        return response() -> json($response, $code);
    }

    public static function throw($message, $code=HttpError::INTERNAL_ERROR) {
        abort($code, $message);
    }

    public static function sendResponse($type, $data, $message='', $code=HttpError::INTERNAL_ERROR) {
        if ($type === 'fail') {
            $response = (object) [
                'err' => true,
                'data' => $data,
                'message' => $message,
                'code' => $code
            ];
            return response() -> json($response, $code);
        }
        elseif ($type === 'success') {
            $response = (object) [
                'success' => true,
                'data' => $data,
                'message' => 'Your Request Has Been Processed',
                'code' => 200
            ];
            return response() -> json($response, 200);
        }
    }
}