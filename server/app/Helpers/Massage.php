<?php
namespace App\Helpers;

class Massage {
  const NOT_FOUND = "Data tidak ditemukan";
  const USER_NOT_FOUND = "User tidak ditemukan";
  const UPDATE_FAILED = "Gagal memperbarui data";
  const DELETED_FAILED = "Gagal menghapus data";
  const ACCOUNT_REGISTERED = "Akun sudah terdaftar";
}