<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Ramsey\Uuid\Uuid as Generator;

class Article extends Model
{
  protected $fillable = [
    'id', 
    'name',
    'title', 
    'content',
    'icon',
    'isActive'
  ];


  protected $casts = [
    'id' => 'string'
  ];

  public static function boot()
  {
    parent::boot();

    static::creating(function ($model) {
      $model->id = Generator::uuid4()->toString();
    });
  }
}
