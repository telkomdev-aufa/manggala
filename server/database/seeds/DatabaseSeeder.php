<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeders.
     *
     * @return void
     */
    public function run()
    {
        $userData = [
            [
                'id' => 'dsac3319-1c32-43a0-9a66-a825f4537d90',
                'username' => 'admin',
                'password' => Hash::make('admin')
            ]
        ];
        
        $articleData = [
            [
                'id' => Str::random(3).'13319-1c32-43a0-9a66-a825f4537d90',
                'name' => Str::random(3).'dsa',
                'title' => Str::random(3).' article category',
                'content' => Str::random(3).' Descripaion',
                'isActive' => false,
            ],
            // [
            //     'id' => Str::random(3).'d2bef-0289-4489-8c3c-e838f8a46e18',
            //     'name' => Str::random(3).'dsa',
            //     'title' => Str::random(3).' article category',
            //     'content' => Str::random(3).' Descript1on',
            //     'isActive' => false,
            // ]
        ];

        $imageData = [
            [
                'id' => Str::random(3).'13319-1c32-43a0-9a66-a825f4507d90',
                'image' => Str::random(3).' Descriptdon'
            ],
            [
                'id' => Str::random(3).'d2bef-0289-4489-8c3c-e838f8aa6e18',
                'image' => Str::random(3).' Descriptian'
            ]
        ];


        DB::table('users')->insert($userData);
        DB::table('articles')->insert($articleData);
        DB::table('images')->insert($imageData);
    }
}
